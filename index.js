// **Iteración #1: Interacción con el DOM**
window.onload = onInit;

function onInit() {
    createUl();
    createLiFromArray();
    createUlTwo();
    createLiForUlTwo()
    createDivs();

    deleteLastItemButton();
    deleteLastItem();

    createElementButton();
    deleteSelectedItem();
}


var body = document.querySelector('body');

// 1.1 Basandote en el array siguiente, crea una lista ul > li dinámicamente en el html que imprima cada uno de los paises.
var $$ulList = document.createElement('ul');
const createUl = () => {
    $$ulList.className = 'Clase Ul';
    $$ulList.id = 'ulList';
    $$ulList.style = 'margin-top: 1em;border: 1px solid red; height:100px; display: flex; align-items: center; justify-content: space-evenly;';
    body.appendChild($$ulList);
}

const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];

function createLiFromArray() {
    for (var i = 0; i < countries.length; i++) {
        var $$itemLi = document.createElement('li');
        $$ulList.appendChild($$itemLi);
        $$itemLi.className = 'itemLi';
        $$itemLi.style = 'width: 20%, display: flex; align-content:center;';
        var country = countries[i];

        var $$liAnchor = document.createElement('a');
        $$itemLi.appendChild($$liAnchor);
        $$liAnchor.innerHTML = country;
        $$liAnchor.style = "color:#123456;";
    }
}
// 1.2 Elimina el elemento que tenga la clase .fn-remove-me.

const elements = document.getElementsByClassName('fn-remove-me');
function removeElementsByClass() {
    while (elements.length > 0) {
        elements[0].parentNode.removeChild(elements[0]);
    }
}
removeElementsByClass();


function onRemove() {
    if (body.classList.contains('fn-remove-me')) {
        console.log('This class exists');
    } else {
        console.log('This class has been removed');
    }
}
onRemove()
// 1.3 Utiliza el array para crear dinamicamente una lista ul > li de elemento en el div de html con el atributo data-function="printHere".
const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];

var $$ulListTwo = document.createElement('ul');
var atribute = document.querySelector("[data-function=printHere]");

const createUlTwo = () => {
    $$ulListTwo.className = 'Clase Ul';
    $$ulListTwo.id = 'ulListTwo';
    $$ulListTwo.style = 'margin-top: 1em;border: 1px solid black; height:100px; display: flex; align-items: center; justify-content: space-evenly;';
    // body.appendChild($$ulList);
    // insertAfter($$ulListTwo, $$ulList)
    if (atribute !== null) {
        atribute.appendChild($$ulListTwo);
    }
}

function createLiForUlTwo() {
    for (var i = 0; i < cars.length; i++) {
        var $$itemLiTwo = document.createElement('li');
        $$ulListTwo.appendChild($$itemLiTwo);
        $$itemLiTwo.className = 'itemLi';
        $$itemLiTwo.style = 'width: 20%, display: flex; align-content:center;';
        var car = cars[i];

        var $$liAnchor = document.createElement('a');
        $$itemLiTwo.appendChild($$liAnchor);
        $$liAnchor.innerHTML = car;
        $$liAnchor.style = "color:#123456;";
    }
}

function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
// 1.4 Crea dinamicamente en el html una lista de div que contenga un elementoh4 para el titulo y otro elemento img para la imagen.
const array4 = [
    { title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1' },
    { title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2' },
    { title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3' },
    { title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4' },
    { title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5' }
];

function createDivs() {
    let $$createDiv = document.createElement('div');
    $$createDiv.className = 'DivCreated';
    $$createDiv.style = 'border: 1px solid blue;display: flex;justify-content: space-around;align-items: center;';
    body.appendChild($$createDiv);

    for (value of array4) {
        //   if (array4.length >= array4.length -1 ) {
        // console.log(value);   POR CONSOLA ME DA 5 PERO ME FALLA EN ALGO LA ITERACION DEL FOR OF Y SOLO PINTA 4, falta el último
        var $$elementDiv = document.createElement('div');
        $$elementDiv.className = 'elementToDelete';
        $$elementDiv.id = `elemento-${array4.indexOf(value)}`
        $$elementDiv.style = 'border: 1px dotted pink;'

        var $$title = document.createElement('h4');
        // .textContent añade el texto al p o h creado !!!
        $$title.textContent = value.title;
        $$title.style = 'text-align:center;'
        $$elementDiv.appendChild($$title);

        var $$imgUrl = document.createElement('img');
        // src y le doy el valor del array que quiero al div
        $$imgUrl.src = value.imgUrl;
        $$imgUrl.style = 'height: 50px; display: flex;margin: 1rem;'
        $$elementDiv.appendChild($$imgUrl);

        $$createDiv.appendChild($$elementDiv);
        // console.log($$elementDiv)
        // } 
    }
}


// 1.5 Basandote en el ejercicio anterior. Crea un botón que elimine el último
// elemento de la lista.
function deleteLastItemButton() {
    var $$button = document.createElement('button');
    $$button.id = 'button-delete';
    $$button.textContent = 'Delete last';
    $$button.style = 'display: block;margin: 1em auto';
    body.appendChild($$button);
    $$button.addEventListener('click', deleteLastItem);
}

function deleteLastItem() {
    var $$deleteLast = document.querySelector('.DivCreated');
    $$deleteLast.removeChild($$deleteLast.lastChild)
}
// 1.6 Basandote en el ejercicio anterior. Crea un botón para cada uno de los
// elementos de las listas que elimine ese mismo elemento del html.
function createElementButton() {
    const $$elementDiv = document.getElementsByClassName('elementToDelete');

    [...$$elementDiv].forEach((parent, i) => {
        const $$buttonElement = document.createElement('button');
        $$buttonElement.className = 'button-remove';
        $$buttonElement.id = `button-element-${[i]};`
        $$buttonElement.textContent = 'delete element';
        $$buttonElement.style = 'display: block;margin: 1em auto';
        $$buttonElement.addEventListener('click', deleteSelectedItem);
        parent.appendChild($$buttonElement);
    });
};
function deleteSelectedItem(wholeDiv) {
    var $$btn = document.getElementsByClassName('button-remove')
    for (var i = 0; i < $$btn.length; i++) {
        
        wholeDiv.currentTarget.parentNode.remove();
    }
};





